#ifndef CELL_H
#define CELL_H

#include <ctime>
#include <random>
#include <QPushButton>
#include <QObject>
#include <QMouseEvent>
#include <QWidget>
#include <QGridLayout>
#include <QSignalMapper>
#include <QGroupBox>

class Cell : public QPushButton
{
    Q_OBJECT
public:
    void setStatus(int stat);
    int getStatus();
    void plusStatus();

private:
    int status_;

private slots:
    void mouseDoubleClickEvent(QMouseEvent *e);
    void mousePressEvent(QMouseEvent *e);

signals:
    void rightClicked();
    void leftClicked();
    void leftDoubleClicked();
};

#endif // CELL_H
