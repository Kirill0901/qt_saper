#ifndef FIELD_H
#define FIELD_H

#include "cell.h"
#include <QMessageBox>

class Field : public QObject
{
    Q_OBJECT
public:
    Field(int height_in_cells, int width_in_cells, int all_mines, 
          int pixel_size, QWidget *under_field_widget, QWidget *main_window);

    ~Field();

    void newGame(int height_in_cells, int width_in_cells, int all_mines);

    int getCountMine();
    bool getStartStatus();
    void setStartGame();

    void resizeField(int pixel_size);

private:

    void openCell(int x, int y);
    void centerLayout();
    void generateField();
    void endGame();
    void clearField();

    int height_in_cells_, width_in_cells_;

    int all_mines_;
    int mine_;
    int closed_cells_;

    bool is_start_;

    int pixel_size_;

    QGroupBox *field_widget_;

    QWidget *under_field_widget_;

    QWidget *main_window_;

    QGridLayout *field_grid_layout_, *under_grid_layout_;

    QSignalMapper *signal_mapper_left_, *signal_mapper_right_, *signal_mapper_double_left_;

    Cell ***cells_;

private slots:
    void slotOpenCell(int k);
    void slotOpenCellsAround(int k);
    void putFlag(int k);

signals:

};

#endif // FIELD_H
