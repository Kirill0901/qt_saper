#include "settingswidget.h"

SettingsWidget::SettingsWidget(QWidget *parent, QWidget *main_window):
    QWidget(parent), main_window_(main_window)
{

    this->setFixedSize(400, 200);
    this->setWindowTitle("Settings");

    settings_layout_ = new QGridLayout(this);
    this->setLayout(settings_layout_);

    easy_mode_ = new QRadioButton("Easy", this);
    normal_mode_ = new QRadioButton("Normal", this);
    hard_mode_ = new QRadioButton("Hard", this);
    user_mode_ = new QRadioButton("Special", this);

    easy_mode_->setChecked(true);
    name_mode_ = "Easy";

    accept_settings_ = new QPushButton("Accept", this);
    cancel_settings_ = new QPushButton("Cancel", this);

    width_label_ = new QLabel("Width:", this);
    height_label_ = new QLabel("Height:", this);
    mines_label_ = new QLabel("Mines:", this);

    width_ = new QSpinBox(this);
    height_ = new QSpinBox(this);
    mines_ = new QSpinBox(this);

    width_->setRange(2, 30);
    height_->setRange(2, 30);
    mines_->setRange(1, 900);

    width_->setValue(9);
    height_->setValue(9);
    mines_->setValue(10);

    width_->setEnabled(false);
    height_->setEnabled(false);
    mines_->setEnabled(false);

    settings_layout_->addWidget(easy_mode_, 0, 0);
    settings_layout_->addWidget(normal_mode_, 1, 0);
    settings_layout_->addWidget(hard_mode_, 2, 0);
    settings_layout_->addWidget(user_mode_, 3, 0);

    settings_layout_->addWidget(width_label_, 0, 1);
    settings_layout_->addWidget(height_label_, 1, 1);
    settings_layout_->addWidget(mines_label_, 2, 1);

    settings_layout_->addWidget(width_, 0, 2);
    settings_layout_->addWidget(height_, 1, 2);
    settings_layout_->addWidget(mines_, 2, 2);

    settings_layout_->addWidget(accept_settings_, 4, 0);
    settings_layout_->addWidget(cancel_settings_, 4, 2);
    this->setHidden(true);

    QObject::connect(easy_mode_, SIGNAL(clicked(bool)),
                     this, SLOT(setEasy()));
    QObject::connect(normal_mode_, SIGNAL(clicked(bool)),
                     this, SLOT(setNormal()));
    QObject::connect(hard_mode_, SIGNAL(clicked(bool)),
                     this, SLOT(setHard()));
    QObject::connect(user_mode_, SIGNAL(clicked(bool)),
                     this, SLOT(setUser()));
    QObject::connect(accept_settings_, SIGNAL(clicked(bool)),
                     main_window_, SLOT(acceptSettings()));
    QObject::connect(cancel_settings_, SIGNAL(clicked(bool)),
                     main_window_, SLOT(closeSettings()));
    QObject::connect(this, SIGNAL(closed()),
                     main_window_, SLOT(closeSettings()));
}

void SettingsWidget::closeEvent(QCloseEvent *event)
{
    emit closed();
}

void SettingsWidget::getSettings(int &height_in_cells, int &width_in_cells,
                                 int &all_mines, QString &name_mode)
{
    height_in_cells = height_->value();
    width_in_cells = width_->value();
    all_mines = mines_->value();

    name_mode = name_mode_;
}

void SettingsWidget::setEasy()
{
    width_->setEnabled(false);
    height_->setEnabled(false);
    mines_->setEnabled(false);

    width_->setValue(9);
    height_->setValue(9);
    mines_->setValue(10);

    name_mode_ = "Easy";
}

void SettingsWidget::setNormal()
{
    width_->setEnabled(false);
    height_->setEnabled(false);
    mines_->setEnabled(false);

    width_->setValue(16);
    height_->setValue(16);
    mines_->setValue(40);

    name_mode_ = "Normal";
}

void SettingsWidget::setHard()
{
    width_->setEnabled(false);
    height_->setEnabled(false);
    mines_->setEnabled(false);

    width_->setValue(30);
    height_->setValue(16);
    mines_->setValue(99);

    name_mode_ = "Hard";
}

void SettingsWidget::setUser()
{
    width_->setEnabled(true);
    height_->setEnabled(true);
    mines_->setEnabled(true);

    name_mode_ = "Special";
}

SettingsWidget::~SettingsWidget()
{
    delete settings_layout_;

    delete easy_mode_;
    delete normal_mode_;
    delete hard_mode_;
    delete user_mode_;

    delete accept_settings_;
    delete cancel_settings_;

    delete height_label_;
    delete width_label_;
    delete mines_label_;

    delete height_;
    delete width_;
    delete mines_;
}
