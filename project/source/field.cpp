#include "field.h"
#include <iostream>

Field::Field(int height_in_cells, int width_in_cells, int all_mines,
             int pixel_size, QWidget *under_field_widget, QWidget *main_window):
    height_in_cells_(height_in_cells), width_in_cells_(width_in_cells),
    all_mines_(all_mines), pixel_size_(pixel_size), under_field_widget_(under_field_widget), main_window_(main_window)
{
    under_grid_layout_ = new QGridLayout(under_field_widget_);
    field_widget_ = new QGroupBox(under_field_widget_);

    under_grid_layout_->addWidget(field_widget_, 0, 0);
    under_field_widget_->setLayout(under_grid_layout_);
    under_field_widget_->show();

    field_grid_layout_ = new QGridLayout(field_widget_);
    field_widget_->setLayout(field_grid_layout_);

    cells_ = nullptr;
    is_start_ = true;
}

void Field::newGame(int height_in_cells, int width_in_cells, int all_mines)
{
    if (cells_ != nullptr)
        this->clearField();

    height_in_cells_ = height_in_cells;
    width_in_cells_ = width_in_cells;
    all_mines_ = all_mines;
    mine_ = all_mines_;
    closed_cells_ = height_in_cells_ * width_in_cells_;
    is_start_ = true;

    cells_ = new Cell**[height_in_cells_];
    for (int i = 0; i < height_in_cells_; ++i)
    {
        cells_[i] = new Cell*[width_in_cells_];
    }

    signal_mapper_left_ = new QSignalMapper(this);
    signal_mapper_right_ = new QSignalMapper(this);
    signal_mapper_double_left_ = new QSignalMapper(this);

    for (int i = 0; i < height_in_cells; ++i)
    {
        for (int j = 0; j < width_in_cells; ++j)
        {
            cells_[i][j] = new Cell;
            cells_[i][j]->setText("");
            cells_[i][j]->setStatus(0);
            cells_[i][j]->setMaximumSize(pixel_size_, pixel_size_);

            field_grid_layout_->addWidget(cells_[i][j], i, j);

            signal_mapper_left_->
                    setMapping(cells_[i][j], i * width_in_cells_ + j);
            signal_mapper_right_->
                    setMapping(cells_[i][j], i * width_in_cells_ + j);
            signal_mapper_double_left_->
                    setMapping(cells_[i][j], i * width_in_cells_ + j);

            QObject::connect(cells_[i][j], SIGNAL(leftClicked()),
                             signal_mapper_left_, SLOT(map()));
            QObject::connect(cells_[i][j], SIGNAL(rightClicked()),
                             signal_mapper_right_, SLOT(map()));
            QObject::connect(cells_[i][j], SIGNAL(leftDoubleClicked()),
                             signal_mapper_double_left_, SLOT(map()));
        }
    }
    QObject::connect(signal_mapper_left_, SIGNAL(mapped(int)),
                     main_window_, SLOT(tryStartTimer()));
    QObject::connect(signal_mapper_left_, SIGNAL(mapped(int)),
                     this, SLOT(slotOpenCell(int)));
    QObject::connect(signal_mapper_right_, SIGNAL(mapped(int)),
                     this, SLOT(putFlag(int)));
    QObject::connect(signal_mapper_right_, SIGNAL(mapped(int)),
                     main_window_, SLOT(updateMineCounter()));
    QObject::connect(signal_mapper_right_, SIGNAL(mapped(int)),
                     main_window_, SLOT(tryStartTimer())); 
    QObject::connect(signal_mapper_double_left_, SIGNAL(mapped(int)),
                     this, SLOT(slotOpenCellsAround(int)));

    this->generateField();

    this->centerLayout();
}

int Field::getCountMine()
{
    return mine_;
}

bool Field:: getStartStatus()
{
    return is_start_;
}

void Field::setStartGame()
{
    is_start_ = false;
}

void Field::endGame()
{
    for (int i = 0; i < height_in_cells_; ++i)
    {
        for (int j = 0; j < width_in_cells_; ++j)
        {
            if (cells_[i][j]->getStatus() == -1)
            {
                if (closed_cells_ == all_mines_)
                    cells_[i][j]->setText("V");
                else
                    cells_[i][j]->setText("X");
            }
            cells_[i][j]->setEnabled(false);
        }
    }
    if (closed_cells_ == all_mines_)
    {
        QMessageBox msgBox;
        msgBox.setText("You WIN!!!");
        msgBox.exec();
    }
}

void Field::slotOpenCellsAround(int k)
{
    int x = k / width_in_cells_;
    int y = k % width_in_cells_;
    if (cells_[x][y]->getStatus() == -1)
    {
        this->slotOpenCell(k);
    }

    int count = 0;
    for (int dx = -1; dx <= 1; ++dx)
    {
        for (int dy = -1; dy <= 1; ++dy)
        {
            if (x + dx >= 0 && x + dx < height_in_cells_ &&
                y + dy >= 0 && y + dy < width_in_cells_ &&
                cells_[x + dx][y + dy]->text() == "!" &&
                (dx != 0 || dy != 0))
            {
                ++count;
            }
        }
    }

    if (count == cells_[x][y]->getStatus())
    for (int dx = -1; dx <= 1; ++dx)
    {
        for (int dy = -1; dy <= 1; ++dy)
        {
            if (x + dx >= 0 && x + dx < height_in_cells_ &&
                y + dy >= 0 && y + dy < width_in_cells_ &&
                cells_[x + dx][y + dy]->text() == "" &&
                (dx != 0 || dy != 0))
            {
                this->slotOpenCell((x + dx) * width_in_cells_ + y + dy);
            }
        }
    }
}

void Field::slotOpenCell(int k)
{
    int x = k / width_in_cells_;
    int y = k % width_in_cells_;

    if (cells_[x][y]->text() != "")
    {
        return;
    }

    if (cells_[x][y]->getStatus() == -1)
    {
        is_start_ = true;
        this->endGame();
        return;
    }
    cells_[x][y]->setText(QString::number(cells_[x][y]->getStatus()));
    --closed_cells_;
    this->openCell(x, y);

    if (closed_cells_ == all_mines_)
    {
        is_start_ = true;
        this->endGame();
    }
}

void Field::putFlag(int k)
{
    int x = k / width_in_cells_;
    int y = k % width_in_cells_;
    if (cells_[x][y]->text() == "" && mine_ > 0)
    {
        cells_[x][y]->setText("!");
        --mine_;
    }
    else if (cells_[x][y]->text() == "!")
    {
        cells_[x][y]->setText("");
        ++mine_;
    }
}

void Field::openCell(int x, int y)
{
    for (int dx = -1; dx <= 1; ++dx)
    {
        for (int dy = -1; dy <= 1; ++dy)
        {
            if (x + dx >= 0 && x + dx < height_in_cells_ &&
                y + dy >= 0 && y + dy < width_in_cells_ &&
                cells_[x + dx][y + dy]->text() == "" &&
                cells_[x][y]->getStatus() == 0 &&
                (dx != 0 || dy != 0))
            {
                cells_[x + dx][y + dy]->
                            setText(QString::number(cells_[x + dx][y + dy]->getStatus()));
                --closed_cells_;
                if (cells_[x + dx][y + dy]->getStatus() == 0)
                {
                    this->openCell(x + dx, y + dy);
                }
            }
        }
    }
}

void Field::resizeField(int pixel_size)
{
    pixel_size_ = pixel_size;

    for (int i = 0; i < height_in_cells_; ++i)
    {
        for (int j = 0; j < width_in_cells_; ++j)
        {
            cells_[i][j]->setMaximumSize(pixel_size_, pixel_size_);
        }
    }

    centerLayout();
}

void Field::centerLayout()
{
    field_grid_layout_->setSpacing(0);
    field_grid_layout_->setContentsMargins(0, 0, 0, 0);

    under_field_widget_->setMinimumSize(pixel_size_ * width_in_cells_, pixel_size_ * height_in_cells_);
    field_widget_->setMinimumSize(pixel_size_ * width_in_cells_, pixel_size_ * height_in_cells_);
    field_widget_->setMaximumSize(pixel_size_ * width_in_cells_, pixel_size_ * height_in_cells_);

    under_field_widget_->show();
    field_widget_->show();
}

void Field::generateField()
{
    srand(time(NULL));
    for (int i = 0; i < all_mines_; ++i)
    {
        int x =  rand() % height_in_cells_;
        int y = rand() % width_in_cells_;
        while(cells_[x][y]->getStatus() != 0)
        {
            x =  rand() % height_in_cells_;
            y = rand() % width_in_cells_;
        }
        cells_[x][y]->setStatus(-1);
    }

    for (int i = 0; i < height_in_cells_; ++i)
    {
        for (int j = 0; j < width_in_cells_; ++j)
        {
            if (cells_[i][j]->getStatus() == 0)
            {
                for (int x = -1; x <= 1; ++x)
                {
                    for (int y = -1; y <= 1; ++y)
                    {
                        if (i + x >= 0 && i + x < height_in_cells_ &&
                            j + y >= 0 && j + y < width_in_cells_ &&
                            cells_[i + x][j + y]->getStatus() == -1)
                        {
                            cells_[i][j]->plusStatus();
                        }
                    }
                }
            }
        }
    }
}

void Field::clearField()
{
    for (int i = 0; i < height_in_cells_; ++i)
    {
        for (int j = 0; j < width_in_cells_; ++j){
            delete cells_[i][j];
        }
    }
    for (int i = 0; i < height_in_cells_; ++i){
        delete [] cells_[i];
    }
    delete [] cells_;
    delete signal_mapper_left_;
    delete signal_mapper_right_;
    delete signal_mapper_double_left_;
}

Field::~Field()
{
    clearField();
    delete field_grid_layout_;
    delete field_widget_;
    delete under_grid_layout_;
}
