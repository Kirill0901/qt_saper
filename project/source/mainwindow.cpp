#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QVBoxLayout>
#include <iostream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    height_in_cells_ = 9;
    width_in_cells_ = 9;
    all_mines_ = 10;

    field_ = new Field(1, 1, 1, 25, ui->group_box_, this);
    
    QObject::connect(ui->startGame, SIGNAL(clicked(bool)), this, SLOT(newGame()));
    newGame();

    timer = new QTimer(this);
    timer->setInterval(1000);
    QObject::connect(timer, SIGNAL(timeout()), this, SLOT(updateTime()));
    timer->start();
    time_start_game_ = QTime(0,0);

    settings_widget_ = new SettingsWidget(nullptr, this);
    QObject::connect(ui->settings, SIGNAL(clicked(bool)), this, SLOT(openSettings()));
}

void MainWindow::openSettings()
{
    settings_widget_->setHidden(false);
    settings_widget_->show();
}

void MainWindow::closeSettings()
{
    settings_widget_->setHidden(true);
}

void MainWindow::acceptSettings()
{
    QString name_mode;
    settings_widget_->getSettings(height_in_cells_, width_in_cells_, all_mines_, name_mode);
    ui->group_box_->setTitle(name_mode);
    closeSettings();
    newGame();
    this->resizeEvent(nullptr);
}

void MainWindow::updateMineCounter()
{
    QString str = "mine : ";
    str += QString::number(field_->getCountMine());
    ui->mine_counter->setText(str);
}

void MainWindow::resizeEvent(QResizeEvent *event)
{
    if (height() / height_in_cells_ < width() / width_in_cells_)
    {
        field_->resizeField(height() / height_in_cells_ * 0.8);
    }
    else
    {
        field_->resizeField(width() / width_in_cells_ * 0.8);
    }
}

void MainWindow::updateTime()
{
    if (field_->getStartStatus() == false)
    {
        time_start_game_ = time_start_game_.addSecs(1);
    }
    ui->timer->setText((time_start_game_).toString());
}

void MainWindow::tryStartTimer()
{
    if (field_->getStartStatus() == true)
    {
        field_->setStartGame();
    }
}

void MainWindow::newGame()
{
    if (field_ != nullptr)
    {
        field_->newGame(height_in_cells_, width_in_cells_, all_mines_);
        time_start_game_ = QTime(0,0);
        updateMineCounter();
    }
}

MainWindow::~MainWindow()
{
    delete timer;
    delete field_;
    delete settings_widget_;
    delete ui;
}
