#-------------------------------------------------
#
# Project created by QtCreator 2021-01-20T20:02:55
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = saper
TEMPLATE = app

VPATH = project/source
INCLUDEPATH = project/include

SOURCES += project/source/*.cpp

HEADERS += project/include/*.h

FORMS    += mainwindow.ui
